export const environment = {
  production: true,
  apiUrl: 'https://qr-identification.firebaseio.com/',
  firebase: {
    apiKey: "AIzaSyBGRKn38uZ-gjRp_OsISOk3XulKGr5-yzo",
    authDomain: "qr-identification.firebaseapp.com",
    databaseURL: "https://qr-identification.firebaseio.com",
    projectId: "qr-identification",
    storageBucket: "qr-identification.appspot.com",
    messagingSenderId: "243613300161"
  }
};
